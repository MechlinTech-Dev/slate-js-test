import {Card, CardContent} from "@material-ui/core"
import React from "react"

import {SlateContainer} from "./slate/Slate"

export const Editor = () => {
    return (
        <Card className="editor-card">
            <CardContent>
                <SlateContainer />
            </CardContent>
        </Card>
    )
}
