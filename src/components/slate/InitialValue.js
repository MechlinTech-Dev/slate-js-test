export const initialValue = [{
    type: "paragraph",
    children: [{text: "Material Design has standardized over 1,100 official icons, each in five different 'themes' (see below). For each SVG icon, we export the respective React component from the @material-ui/icons package. You can search the full list of these icons."}]
}]
