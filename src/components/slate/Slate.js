import React, {useEffect, useMemo, useState} from "react";
// Import the Slate editor factory.
import {createEditor} from 'slate'

// Import the Slate components and React plugin.
import {Slate, Editable, withReact} from 'slate-react'
import {renderElement} from "./Element";
import {initialValue} from "./InitialValue";
import {TBar} from "./TBar";

export const SlateContainer = () => {
    const editor = useMemo(() => withReact(createEditor()), [])
    const [value, setValue] = useState(initialValue)

    return (
        <Slate value={value} editor={editor} onChange={newVal => setValue(newVal)}>
            <TBar />
            <Editable renderElement={renderElement} />
        </Slate>
    )
}
