import React from "react"
import {Editor, Transforms} from "slate";
import {useSlate} from "slate-react";

import {CONTROLS} from "./CONTROLS"
import {TButton} from "./TButton";

export const TBar = () => {
    const editor = useSlate()
    const handleToolbarClick = (type) => {
        type === "columns" && handleColumns(editor)
        type === "block" && handleBlock(editor)
    }
    return (<div className="toolbar">
        {CONTROLS.map(tool =>

            <TButton handleToolbarClick={handleToolbarClick} type={tool} key={tool} />
        )}
    </div>)
}

const handleColumns = (editor) => {
    Transforms.wrapNodes(editor, {type: "columns"})
    Transforms.splitNodes(editor)
}

const handleBlock = (editor) => {
    const [match] = Editor.nodes(editor, {
        match: n => n.type === "block"
    })
    if (match) {
        Transforms.unwrapNodes(editor, {match: n => n.type === "block"})
    } else {
        Transforms.wrapNodes(editor, {type: "block"})
    }
}
