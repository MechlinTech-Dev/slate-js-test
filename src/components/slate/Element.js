export const renderElement = ({attributes, children, element}) => {
    switch (element.type) {
        case "block":
            let size = 0
            if (attributes.ref.current) {
                size = attributes.ref.current.innerText.length
            }
            return (
                <div
                    {...attributes}
                    style={{border: `${size > 100 ? "4px solid red" : "2px solid black"}`, padding: 10}}
                >
                    {children}
                </div>
            )
        case "columns":
            return (
                <div {...attributes} style={{display: "flex", gap: 10}}>
                    {children}
                </div>
            )
        default:
            return <p {...attributes} style={{flex: "50%"}}>{children}</p>

    }

}
