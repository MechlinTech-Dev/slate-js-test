import {IconButton} from "@material-ui/core"
import ViewColumnIcon from '@material-ui/icons/ViewColumn';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import React from "react"
import {Tooltip} from "@material-ui/core"

export const TButton = ({type, handleToolbarClick}) => {
    let tooltipContent = ""
    switch (type) {
        case "columns":
            tooltipContent = "Divide Into Columns"
            break
        case "block":
            tooltipContent = "Block"
            break
    }
    return (
        <Tooltip title={tooltipContent}>
            <IconButton onClick={() => handleToolbarClick(type)}>
                {type === "columns" && (<ViewColumnIcon fontSize="small" />)}
                {type === "block" && (<CheckBoxOutlineBlankIcon fontSize="small" />)}
            </IconButton>
        </Tooltip>
    )
}
